<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->char('first_line')->nullable();
            $table->char('second_line')->nullable();
            $table->char('city')->nullable();
            $table->char('state')->nullable();
            $table->char('country')->nullable();
            $table->char('pincode')->nullable();
            $table->morphs('addressable');
            $table->char('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
