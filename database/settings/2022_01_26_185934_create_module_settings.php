<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateModuleSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('module.contact_menu_name', 'Contacts');
    }
}
