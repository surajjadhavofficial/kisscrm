<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateGeneralSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.app_name', 'Kiss CRM');

        $this->migrator->add('general.footer_text', 'Worthcoding');

        $this->migrator->add('general.footer_link', 'https://www.worthcoding.com');
    }
}
