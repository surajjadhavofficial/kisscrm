<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Pipeline extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'entity', 'stages', 'initial_stage', 'positive_stage', 'negative_stage'];

    protected $casts = [
        'stages'    => 'collection',
    ];

    protected static function booted()
    {
        static::creating(function ($pipeline) {
            $pipeline->user_id = Auth::user()->id ?? 24;
        });
    }
}
