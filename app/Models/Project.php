<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Project extends Model
{
    use HasFactory;

    public $fillable = ['name', 'contact_id', 'user_id', 'pipeline_id'];

    /**
     * Get the Owner that owns the Project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Owner(): BelongsTo
    {
        return $this->belongsTo(Contact::class);
    }
}
