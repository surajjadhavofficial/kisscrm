<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ProjectResource\Pages;
use App\Models\Project;
use Filament\Forms\Components\BelongsToManyMultiSelect;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\SpatieTagsInput;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class ProjectResource extends Resource
{
    protected static ?string $model = Project::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make(3)->schema([
                    Group::make()->schema([
                        Card::make()->schema([
                            TextInput::make('name')->label('Project Name')->required(),
                            RichEditor::make('description')->disableToolbarButtons([
                                'attachFiles',
                                'codeBlock',
                            ]),
                        ])->columnSpan(2),
                        Card::make()->schema([
                            KeyValue::make('extra_attributes'),
                        ])->columnSpan(2),

                        // BelongsToManyMultiSelect::make('users'),
                    ])->columnSpan(2),
                    Group::make()->schema([
                        Card::make()->schema([
                            Select::make('payment_type')->label('Payment Type')->options([
                                'Lumpsum'   => 'Lumpsum',
                                'Upfront'   => 'Upfront',
                                'Hourly'    => 'Hourly',
                                'Milestone' => 'Milestone',
                                'Metered'   => 'Metered',
                            ]),
                            Grid::make()->schema([
                                TextInput::make('unit_value')->label('Unit Price : $')->numeric(),
                                TextInput::make('project_value')->label('Project Value : $')->numeric(),
                            ]),
                        ]),
                        // Card::make()->schema([
                        //     SpatieTagsInput::make('tags')->type('project'),
                        // ]),
                    ])->columnSpan(1),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('name'),
                Tables\Columns\TextColumn::make('unit_price')->money('usd'),
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListProjects::route('/'),
            'create' => Pages\CreateProject::route('/create'),
            'edit'   => Pages\EditProject::route('/{record}/edit'),
        ];
    }
}
