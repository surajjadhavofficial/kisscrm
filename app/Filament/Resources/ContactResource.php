<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ContactResource\Pages;
use App\Models\Contact;
use Filament\Forms;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\KeyValue;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\Tabs\Tab;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;

class ContactResource extends Resource
{
    protected static ?string $model = Contact::class;

    protected static ?string $navigationIcon = 'heroicon-o-user';

    // public static function form(Form $form): Form
    // {
    //     return $form
    //         ->schema([
    //             Forms\Components\TextInput::make('salutation')
    //                 ->maxLength(255),
    //             Forms\Components\TextInput::make('first_name')
    //                 ->maxLength(255),
    //             Forms\Components\TextInput::make('last_name')
    //                 ->maxLength(255),
    //             Forms\Components\Repeater::make('email')->schema([
    //                 Select::make('type')->options([
    //                   'Primary','Home','Office'
    //                 ])->required(),
    //                 TextInput::make('email')->email()->required(),
    //             ]),
    //                 // ->email(),
    //             Forms\Components\TextInput::make('contact_number'),
    //             Forms\Components\TextInput::make('gender')
    //                 ->required()
    //                 ->maxLength(255),
    //             Forms\Components\TextInput::make('type')
    //                 ->required()
    //                 ->maxLength(255),
    //         ]);
    // }

    public static function form(Form $form): Form
    {
        return $form
            ->schema(static::getFormSchema(Forms\Components\Card::class))
            ->columns(3);
    }

    public static function getFormSchema(string $layout = Forms\Components\Grid::class): array
    {
        return [
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('Basic Info'),
                            Grid::make(12)->schema([

                                Select::make('salutation')
                                    ->options([])
                                    ->label(__('Salutation'))
                                    ->columnSpan(2),

                                Forms\Components\TextInput::make('first_name')
                                    ->required()
                                    ->label(__('First Name'))
                                    ->columnSpan(5),

                                Forms\Components\TextInput::make('last_name')
                                    ->label(__('Last Name'))
                                    ->required()->columnSpan(5),
                            ]),

                            Grid::make()->schema([
                                TextInput::make('email')->label('Email')->email()->required(),
                                TextInput::make('contact_number')->label('Contact Number')->numeric()->required(),
                            ]),

                        ])->columns([
                            'sm' => 2,
                        ]),

                    Group::make()->schema([
                        Tabs::make('Extra')->schema([
                            Tab::make('Bio')->schema([
                                Textarea::make('bio')
                                    ->label('')
                                    ->rows(4)
                                    ->maxLength(240)
                                    ->columnSpan(2),
                            ]),

                            Tab::make('Custom Fields')->schema([
                                KeyValue::make('extra')
                                        ->label('')
                                        ->keyLabel('Parameter')
                                        ->valueLabel('Values')
                                        ->addButtonLabel('Add Custom Field'),
                            ]),
                        ])->columnSpan(2),
                    ]),

                ])->columnSpan(2),

            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([

                            SpatieMediaLibraryFileUpload::make('avatar')->image(),

                            Forms\Components\DatePicker::make('published_at')
                                ->label('Availability')
                                ->default(now())
                                ->required(),
                        ])
                        ->columns(1),
                ])
                ->columnSpan(1),
        ];
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('salutation'),
                Tables\Columns\TextColumn::make('first_name'),
                Tables\Columns\TextColumn::make('last_name'),
                Tables\Columns\TextColumn::make('email'),
                Tables\Columns\TextColumn::make('contact_number'),
                Tables\Columns\TextColumn::make('gender'),
                Tables\Columns\TextColumn::make('type'),
                Tables\Columns\TextColumn::make('deleted_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('created_at')
                    ->dateTime(),
                Tables\Columns\TextColumn::make('updated_at')
                    ->dateTime(),
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListContacts::route('/'),
            'create' => Pages\CreateContact::route('/create'),
            'view'   => Pages\ViewContact::route('/{record}'),
            'edit'   => Pages\EditContact::route('/{record}/edit'),
        ];
    }
}
