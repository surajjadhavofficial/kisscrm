<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PipelineResource\Pages;
use App\Models\Pipeline;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Repeater;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Illuminate\Support\Str;

class PipelineResource extends Resource
{
    protected static ?string $model = Pipeline::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make()->schema([
                    TextInput::make('name')->label('Pipeline Name'),
                    Select::make('entity')->label('Assign To Entity')->options(
                        [
                            'Lead'    => 'Lead',
                            'Project' => 'Project',
                            'Task'    => 'Task',
                            'Order'   => 'Order',
                        ]
                    ),
                ]),

                Grid::make(1)->schema([
                    Repeater::make('stages')->schema([
                        Grid::make(3)->schema([
                            TextInput::make('stage')->label('Stage Name')->required(),
                            TextInput::make('description'),
                            TextInput::make('possibility')->required()->numeric(),
                        ]),
                    ])->reactive()->lazy()->defaultItems(0)->label('')->createItemButtonLabel('Add Stages to Pipeline'),
                ]),
                Card::make()->schema([
                    Placeholder::make('Label default stages'),
                    Grid::make(3)->schema([
                        Select::make('initial_stage')->options(function (callable $get) {
                            $options = [];
                            if (!empty($get('stages'))) {
                                foreach ($get('stages') as $key => $value) {
                                    if ($value['stage'] != null && $value['possibility'] != null) {
                                        $options[$value['stage']] = Str::ucfirst($value['stage']);
                                    }
                                }
                            }

                            return $options;
                        })->label('Intial Stage')->hint('Default stage for Pipeline'),

                        Select::make('positive_stage')->options(function (callable $get) {
                            $options = [];
                            if (!empty($get('stages'))) {
                                foreach ($get('stages') as $key => $value) {
                                    if ($value['stage'] != null && $value['possibility'] == 100) {
                                        $options[$value['stage']] = Str::ucfirst($value['stage']);
                                    }
                                }
                            }

                            return $options;
                        })->label('Positive Stage')->hint('Stages with 100% Possibility'),

                        Select::make('negative_stage')->options(function (callable $get) {
                            $options = [];
                            if (!empty($get('stages'))) {
                                foreach ($get('stages') as $key => $value) {
                                    if ($value['stage'] != null && $value['possibility'] == 0) {
                                        $options[$value['stage']] = Str::ucfirst($value['stage']);
                                    }
                                }
                            }

                            return $options;
                        })->label('Negative Stage')->hint('Stages with 0% Possibility'),
                    ]),
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
            ])
            ->filters([
                //
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index'  => Pages\ListPipelines::route('/'),
            'create' => Pages\CreatePipeline::route('/create'),
            'edit'   => Pages\EditPipeline::route('/{record}/edit'),
        ];
    }
}
