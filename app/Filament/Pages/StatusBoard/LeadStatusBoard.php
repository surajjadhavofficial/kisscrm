<?php

namespace App\Filament\Pages\StatusBoard;

use Filament\Pages\Page;
use Illuminate\Contracts\View\View;

class LeadStatusBoard extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static ?string $navigationGroup = 'Status Boards';

    protected static string $view = 'filament.pages.status-board.lead-status-board';

    public $pipeline = 'one';

    // protected function getHeader(): ?View
    // {
    //     return view('filament.settings.custom-header');
    // }
}
