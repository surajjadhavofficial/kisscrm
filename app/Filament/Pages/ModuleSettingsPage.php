<?php

namespace App\Filament\Pages;

use App\Settings\ModuleSettings;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Pages\SettingsPage;

class ModuleSettingsPage extends SettingsPage
{
    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static ?string $navigationGroup = 'Settings';

    protected static string $settings = ModuleSettings::class;

    protected function getFormSchema(): array
    {
        return [

            Grid::make()->schema([
                TextInput::make('contact_menu_name')->placeholder('Ex. Contacts Page'),

                TextInput::make('contact_menu_icon')->placeholder('Ex. heroicon-o-users'),

            ]),

        ];
    }
}
