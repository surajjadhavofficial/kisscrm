<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class ModuleSettings extends Settings
{
    public ?string $contact_menu_name;

    public static function group(): string
    {
        return 'module';
    }
}
