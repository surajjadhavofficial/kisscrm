<?php

namespace App\Http\Livewire;

use App\Models\Pipeline;
use Asantibanez\LivewireStatusBoard\LivewireStatusBoard;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class LeadStatusBoard extends LivewireStatusBoard
{
    public function statuses(): Collection
    {
        $stages = Pipeline::first()->stages;

        return $stages->map(function ($status) {
            return [
                'id'    => $status['stage'],
                'title' => Str::ucfirst($status['stage']),
            ];
        });
    }

    public function records(): Collection
    {
        return collect([
            [
                'id'        => 'ONE',
                'title'     => 'TITLE',
                'status'    => 'pass',
            ],
        ]);
    }

    public function onStatusChanged($recordId, $statusId, $fromOrderedIds, $toOrderedIds)
    {
        dump([$recordId, $statusId, $fromOrderedIds, $toOrderedIds]);
    }
}
