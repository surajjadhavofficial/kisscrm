<?php

namespace App\Http\Requests;

use Orion\Http\Requests\Request;

class ProjectRequest extends Request
{
    public function commonRules(): array
    {
        return [
            'name' => 'numeric',
        ];
    }

    public function storeRules(): array
    {
        return [
            'status' => 'required|in:draft,review',
        ];
    }
}
