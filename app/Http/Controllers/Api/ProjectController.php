<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProjectRequest;
use App\Models\Project;
use Orion\Concerns\DisableAuthorization;
use Orion\Http\Controllers\Controller;

class ProjectController extends Controller
{
    use DisableAuthorization;

    protected $model = Project::class;

    protected $request = ProjectRequest::class;
}
