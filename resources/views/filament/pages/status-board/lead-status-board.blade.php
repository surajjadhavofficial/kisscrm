<x-filament::page>
    <select id="data.pipline" wire:model.defer="data.pipeline"
        class="text-gray-900 block w-60 h-10 duration-75 rounded-lg shadow-sm focus:border-primary-600 focus:ring-1 focus:ring-inset focus:ring-primary-600 border-gray-300">
        <option value="">Select an option</option>
    </select>
    <livewire:lead-status-board :sortable="true" :sortable-between-statuses="true" />
</x-filament::page>
