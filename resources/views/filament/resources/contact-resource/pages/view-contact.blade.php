<x-filament::page>
    @dump([$record,$data])
    
    <livewire:lead-status-board
    :sortable="true"
    :sortable-between-statuses="true"
    />
</x-filament::page>
