@if (config('filament.layout.footer.should_show_logo'))
    <div class="flex items-center justify-center">
        <a
            href=""
            target="_blank"
            rel="noopener noreferrer"
            class="text-gray-300 hover:text-primary-500 transition text-sm"
        >
        </a>
    </div>
@endif
