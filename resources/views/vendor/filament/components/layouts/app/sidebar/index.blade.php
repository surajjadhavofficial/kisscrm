<aside
    x-data="{}"
    x-cloak
    x-bind:class="$store.sidebar.isOpen ? 'translate-x-0' : '-translate-x-full'"
    class="fixed inset-y-0 left-0 rtl:left-auto rtl:right-0 z-20 flex flex-col h-screen overflow-hidden shadow-2xl duration-300 bg-white lg:border-r w-80 lg:z-0 lg:translate-x-0"
>
    <header class="border-b h-[4rem] shrink-0 px-6 flex items-center">
        <a href="{{ url('/') }}">
            <x-filament::brand />
        </a>
    </header>

    <nav class="flex-1 overflow-y-auto py-6">
        <x-filament::layouts.app.sidebar.start />

        <ul class="space-y-6 px-6">
            @foreach (\Filament\Facades\Filament::getNavigation() as $group => $items)
                @if ($group == 'Settings')
                <div x-data="{show: false}" @click.away="show = false">
                    <button @click="show = ! show" class="flex items-center gap-3 px-3 py-2 rounded-lg font-medium transition">
                        <div class="flex">
                            <span>{{ $group }}</span>
                            <svg class="fill-current text-gray-200" xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M7 10l5 5 5-5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
                        </div>
                    </button>
                    <div x-show="show">
                        @foreach ($items as $item)
                        <x-filament::layouts.app.sidebar.item
                            :active="$item->isActive()"
                            :icon="$item->getIcon()"
                            :url="$item->getUrl()"
                        >
                            {{ $item->getLabel() }}
                        </x-filament::layouts.app.sidebar.item>
                    @endforeach
                    </div>
                </div>
                @else
                <x-filament::layouts.app.sidebar.group :label="$group">
                    @foreach ($items as $item)
                        <x-filament::layouts.app.sidebar.item
                            :active="$item->isActive()"
                            :icon="$item->getIcon()"
                            :url="$item->getUrl()"
                        >
                            {{ $item->getLabel() }}
                        </x-filament::layouts.app.sidebar.item>
                    @endforeach
                </x-filament::layouts.app.sidebar.group>
                @endif


                @if (! $loop->last)
                    <li>
                        <div class="border-t -mr-6"></div>
                    </li>
                @endif
            @endforeach
        </ul>

        <x-filament::layouts.app.sidebar.end />
    </nav>

    <x-filament::layouts.app.sidebar.footer />
</aside>
